@extends('layouts.main')

@section('titulo', 'Inicio')

@section('contenido')
    <div class="container my-5 gradient-bg-home">
        <div class="grey-layer"></div>
        <div class="row my-5 hero_wrapper">
            <div class="col-lg-12 text-center">
                <h1 class="title-home">Pagina de Inicio para todos <br> <span class="text-is-gradient"> nuestros Clientes </span></h1>

                <figure class="blockquote">
                    <blockquote class="blockquote">
                        <p>A well-known quote, contained in a blockquote element.</p>
                    </blockquote>
                    <figcaption class="blockquote-footer">
                        Someone famous in <cite title="Source Title">Source Title</cite>
                    </figcaption>
                </figure>

                <button class="btn btn-primary btn-lg">Mostrar Clientes</button>
                <div class="circulo-morado"></div>
            </div>
        </div>

        <div class="row row-cols-1 row-cols-md-3 g-4">
            <div class="col d-flex">
                <div class="card flex-fill">
                    <div class="card-body">
                        <div class="d-flex justify-content-center mt-2 mb-4">
                            <i class="fa-solid fa-users fa-4x gradient-for-icons"></i>
                        </div>

                        <h5 class="card-title">Quienes Somos</h5>
                        <p class="card-text">This is a longer card with supporting text below as a natural lead-in to
                            additional content. This content is a little bit longer.</p>
                        <figcaption class="blockquote-footer">
                            Someone famous in <cite title="Source Title">Source Title</cite>
                        </figcaption>

                        <div class="d-flex justify-content-center mt-2">
                            <a class="btn btn-outline-primary btn-lg " href="#" role="button">Quienes Somos</a>
                        </div>
                    </div>
                    <div class="gradient-for-card-tb"></div>
                </div>
            </div>

            <div class="col d-flex">
                <div class="card flex-fill">
                    <div class="card-body">
                        <div class="d-flex justify-content-center mt-2 mb-4">
                            <i class="fa-solid fa-laptop-file fa-4x gradient-for-icons"></i>
                        </div>

                        <h5 class="card-title">Servicios</h5>
                        <p class="card-text">This is a longer card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
                        <figcaption class="blockquote-footer">
                            Someone famous in <cite title="Source Title">Source Title</cite>
                        </figcaption>

                        <div class="d-flex justify-content-center mt-2">
                            <a class="btn btn-outline-primary btn-lg " href="#" role="button">Servicios</a>
                        </div>
                    </div>
                    <div class="gradient-for-card-tb"></div>
                </div>
            </div>

            <div class="col d-flex">
                <div class="card flex-fill">
                    <div class="d-flex justify-content-center mt-2 mb-4">
                        <i class="fa-brands fa-keybase fa-4x gradient-for-icons"></i>
                    </div>

                    <div class="card-body">
                        <h5 class="card-title">Iniciar Sesión</h5>
                        <p class="card-text">This is a longer card with supporting text below as a natural lead-in to
                            additional content.</p>
                        <figcaption class="blockquote-footer">
                            Someone famous in <cite title="Source Title">Source Title</cite>
                        </figcaption>

                        <div class="d-flex justify-content-center mt-2">
                            <a class="btn btn-outline-primary btn-lg" href="#" role="button">Iniciar Sesión</a>
                        </div>
                    </div>
                    <div class="gradient-for-card-tb"></div>
                </div>
            </div>
        </div>
    </div>
@endsection
