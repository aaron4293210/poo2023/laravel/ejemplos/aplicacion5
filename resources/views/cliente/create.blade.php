@extends('layouts.main')

@section('titulo', 'Crear Clientes')

@section('cabecera')
    @parent
@endsection

@section('contenido')
    @if ($errors->any())
        <div class="card gradient-for-card-se mb-4">
            <div class="card-body text-center" style="background-color: #1b1724">
                <h2 class="card-title font-italic">Error</h2>

                @foreach ($errors->all() as $error)
                    <p class="card-text lead fs-4"> {{ $error }} </p>
                @endforeach
            </div>
        </div>
    @endif

    <div class="row justify-content-center m-0">
        <div class="col-lg-5 d-flex align-items-stretch p-0 d-none d-lg-flex">
            <div class="d-flex p-0">
                <img class="img-fluid d-block mx-auto rounded-start" src="{{ asset('imgs/create_user.jpg') }}" alt="" style="object-fit: cover;"> <!-- Añade 'object-fit: cover' aquí -->
            </div>
        </div>

        <div class="col-lg-7 p-0">
            <div class="card rounded-0 rounded-end">
                <div class="card-body">
                    <h1 class="text-is-gradient text-center">Crear Nuevo Cliente</h1>

                    <form action="{{ route('cliente.store') }}" class="card-body cardbody-color p-lg-5 needs-validation" novalidate method="POST">
                        @csrf
                    
                        @foreach (['nombre', 'apellidos', 'telefono', 'email'] as $campo)
                            <div class="input-group mb-3">
                                <div class="input-group has-validation">
                                    <div class="form-floating">
                                        <input type="{{ $campo === 'email' ? 'email' : 'text' }}" class="form-control" id="{{ $campo }}" name="{{ $campo }}" value="{{ old($campo) }}" required>
                                        <label class="form-label" for="{{ $campo }}">{{ ucfirst($campo) }} <span class="text-danger">*</span></label>
                                        <div id="{{ $campo }}" class="invalid-feedback">
                                            Este campo es obligatorio. {{ $campo === 'email' ? 'Asegurate que el formato sea el correcto' : '' }}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    
                        <div class="input-group mb-3">
                            <div class="input-group has-validation">
                                <div class="form-floating">
                                    <textarea class="form-control" id="direccion" name="direccion" required>{{ old('direccion') }}</textarea>
                                    <label for="direccion">Dirección <span class="text-danger">*</span></label>
                                    <div id="direccion" class="invalid-feedback">
                                        Este campo es obligatorio
                                    </div>
                                </div>
                            </div>
                        </div>
                    
                        <div class="text-center">
                            <button type="submit" class="btn btn-secondary btn-lg">Insertar</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <script>
        document.addEventListener('submit', validacionFormulario);

        function validacionFormulario(e) {
            const formulario = e.target;

            if (!formulario.classList.contains('needs-validation')) {
                return;
            }

            // Si el formulario no es válido, previene el evento de envío y detiene la propagación
            if (!formulario.checkValidity()) {
                e.preventDefault();
                e.stopPropagation();
            }

            formulario.classList.add('was-validated');
        }
    </script>
@endsection
