@extends('layouts.main')

@section('titulo', 'Editar Cliente')

@section('cabecera')
    @parent
@endsection

@section('contenido')
    <ul class="nav nav-tabs mb-5">
        <li class="nav-item">
            <a class="nav-link" aria-current="page" href="{{ route('cliente.show', $cliente->id) }}">Cliente {{ $cliente->id }}</a>
        </li>

        <li class="nav-item">
            <a class="nav-link active disabled" aria-disabled="true" href="{{ route('cliente.edit', $cliente->id) }}">Editar Cliente</a>
        </li>
        
        <li class="nav-item" style="margin-left: auto;">
            <a class="nav-link" aria-current="page" href="{{ route('cliente.show', $cliente->id) }}">Volver</a>
        </li>
    </ul>

    @if (session('mensaje'))
        <div class="card gradient-for-card-se mb-4">
            <div class="card-body text-center" style="background-color: #1b1724">
                <p class="card-text lead fs-4">{{ session('mensaje') }} </p>
            </div>
        </div>
    @endif
    
    @if ($errors->any())
        <div class="card gradient-for-card-se mb-4">
            <div class="card-body text-center" style="background-color: #1b1724">
                <h2 class="card-title font-italic">Error</h2>
                @foreach ($errors->all() as $error)
                    <p class="card-text lead fs-4"> {{ $error }} </p>
                @endforeach
            </div>
        </div>
    @endif

    <div class="row justify-content-center">
        <div class="col-lg-8 card">
            <div class="card-body">
                <h1 class="text-is-gradient text-center">Actualizar Cliente</h1>

                @php
                    $composIncluidos = ['nombre', 'apellidos', 'email', 'telefono'];
                    $campos = collect($cliente->getAttributes())->only($composIncluidos)->keys();
                @endphp

                <form action="{{ route('cliente.update', $cliente) }}" class="card-body cardbody-color p-lg-5 needs-validation" novalidate method="POST">
                    @csrf
                    @method('PUT')

                    @foreach($campos as $campo)
                        <div class="input-group mb-3">
                            <div class="input-group has-validation">
                                <div class="form-floating">
                                    <input type="{{ $campo === 'email' ? 'email' : 'text' }}" class="form-control" id="{{ $campo }}" name="{{ $campo }}" value="{{ old($campo, $cliente->$campo) }}" required>
                                    <label class="form-label" for="{{ $campo }}">{{ ucfirst($campo) }}</label>
                                    <div id="{{ $campo }}" class="invalid-feedback">
                                        Este campo es obligatorio. {{ $campo === 'email' ? 'Asegurate que el formato sea el correcto' : '' }}
                                        @error($campo)
                                            <p style="color: #dc3545;"> {{ $message }} </p>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach

                    <div class="input-group mb-3">
                        <div class="input-group has-validation">
                            <div class="form-floating">
                                <textarea class="form-control" id="direccion" name="direccion" required>{{ old('direccion', $cliente->direccion) }}</textarea>
                                <label for="direccion" class="form-label">Dirección</label>
                                <div id="direccion" class="invalid-feedback">
                                    Este campo es obligatorio
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="text-center">
                        <button type="submit" class="btn btn-secondary btn-lg">Actualizar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <script>
        document.addEventListener('submit', validacionFormulario);

        function validacionFormulario(e) {
            const formulario = e.target;

            if (!formulario.classList.contains('needs-validation')) {
                return;
            }

            // Si el formulario no es válido, previene el evento de envío y detiene la propagación
            if (!formulario.checkValidity()) {
                e.preventDefault();
                e.stopPropagation();
            }

            formulario.classList.add('was-validated');
        }
    </script>
@endsection
