@extends('layouts.main')

@section('titulo', 'Mostrar')

@section('contenido')
<div class="row">
    <ul class="nav nav-tabs">
        <li class="nav-item">
            <a class="nav-link active disabled" aria-disabled="true">Cliente {{ $cliente->id }}</a>
        </li>
    
        <li class="nav-item">
            <a class="nav-link" href="{{ route('cliente.edit', $cliente->id) }}">Editar Cliente</a>
        </li>
        
        <li class="nav-item" style="margin-left: auto;">
            <a class="nav-link" aria-current="page" href="{{ route('cliente.index') }}">Volver</a>
        </li>
    </ul>

    @if (session('mensaje'))
        <div class="card gradient-for-card-se mt-4">
            <div class="card-body text-center" style="background-color: #1b1724">
                <p class="card-text lead fs-4"> {{ session('mensaje') }} </p>
            </div>
        </div>
    @endif
    
    <div class="col-lg-10 my-4 mb-sm-0 offset-lg-1">
        <div class="card text-center listadoTarjetaCliente">
            <div class="card-header">
                <span class="fs-3 text-center text-is-gradient">Cliente {{ $cliente->id }}</span>
            </div>

            <div class="card-body text-center">
                <i class="fa-solid fa-user-astronaut fa-5x profile-pic gradient-for-icons mb-4"></i>
                <h4 class="card-title font-italic nombreListadoCliente my-4">{{ $cliente->nombre }}</h4>
            
                <div class="row">
                    <div class="col-lg-6">
                        <div class="list-group">
                            <div class="list-group-item d-flex justify-content-between align-items-center p-3">
                                <span class="fw-bold text-uppercase">Nombre:</span>
                                <span class="ms-auto">{{ $cliente->nombre }}</span>
                            </div>
                            <div class="list-group-item d-flex justify-content-between align-items-center p-3">
                                <span class="fw-bold text-uppercase">Apellidos:</span>
                                <span class="ms-auto">{{ $cliente->apellidos }}</span>
                            </div>
                            <div class="list-group-item d-flex justify-content-between align-items-center p-3">
                                <span class="fw-bold text-uppercase">Teléfono:</span>
                                <span class="ms-auto">{{ $cliente->telefono }}</span>
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-lg-6">
                        <div class="list-group">
                            <div class="list-group-item d-flex justify-content-between align-items-center p-3">
                                <span class="fw-bold text-uppercase">Email:</span>
                                <span class="text-truncate ms-auto" style="max-width: 100%">{{ $cliente->email }}</span>
                            </div>
                            <div class="list-group-item d-flex justify-content-between align-items-center p-3">
                                <span class="fw-bold text-uppercase">Dirección:</span>
                                <div class="text-end ms-auto">{{ $cliente->direccion }}</div>
                            </div>
                        </div>
                    </div>   
                </div>

                <div class="my-4 d-flex justify-content-center align-items-center">
                    <a class="btn btn-outline-success me-2" href="{{ route('cliente.edit', $cliente->id) }}" role="button">Editar</a>
                    
                    <form action="{{ route('cliente.destroy', $cliente)}}" method="POST">
                        @csrf @method('DELETE')
                        <button type="submit" class="btn btn-outline-danger">Borrar</button>
                    </form>      
                </div>
                
            </div>
        </div>
    </div>
</div>
@endsection