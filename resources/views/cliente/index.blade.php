@extends('layouts.main')

@section('titulo', 'Clientes')

@section('contenido')

@if (session('mensaje'))
    <div class="card gradient-for-card-se mb-2">
        <div class="card-body text-center" style="background-color: #1b1724">
            <p class="card-text lead fs-4"> {{ session('mensaje') }} </p>
        </div>
    </div>
@endif

<div class="row">
    <h1 class="text-is-gradient text-center">Listado de Clientes</h1>
    @foreach ($clientes as $cliente)
        <div class="col-lg-12 mb-sm-0">
            <div class="card listadoTarjetaCliente" style="padding: 0.25rem">
                <div class="card-body d-flex align-items-center">
                    <div class="col-lg-1 mb-sm-0">
                        <a href="{{ route('cliente.show', $cliente->id)}}"><i class="fa-solid fa-user-astronaut fa-2x profile-pic gradient-for-icons"></i></a>
                    </div>

                    <div class="col-lg-8">
                        <h4 class="card-title font-italic nombreListadoCliente">{{ $cliente->nombre }}</h4>
                    </div>

                    <div class="col-lg-3 d-flex justify-content-end align-items-center">
                        <div>
                            <a class="btn btn-outline-primary" href="{{ route('cliente.show', $cliente->id)}}" role="button">Ver</a>
                            <a class="btn btn-outline-success" href="{{ route('cliente.edit', $cliente->id)}}" role="button">Editar</a>
                        </div>
                    
                        <form action="{{ route('cliente.destroy', $cliente)}}" method="POST">
                            @csrf @method('DELETE')
                            <button type="submit" class="btn btn-outline-danger mx-1">Borrar</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    @endforeach
</div>

<div class="row mt-3">
    {{-- Es el paginador --}}
    {{ $clientes->links() }}
</div>
@endsection