<?php

namespace App\Http\Controllers;

use App\Models\Cliente;
use Illuminate\Http\Request;

class ClienteController extends Controller {  
    /**
     * Se envian todos los clientes se cargan de la base de datos.
     * Se mostrara paginado de 15 en 15
     *
    */
    public function index() {
        $clientes = Cliente::paginate(15);

        return view('cliente.index', compact('clientes'));
    }

    /**
     * Muestra todos los detalles de un cliente
     * @param int $id
     */
    public function show(int $id) {
        $cliente = Cliente::find($id);

        return view('cliente.show', compact('cliente'));
    }

    public function create() {
        return view('cliente.create');
    }

    /**
     * Almacena un recurso recién creado en DB.
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
    */
    public function store(Request $request) {
        $request->validate([
            'nombre' => 'required',
            'apellidos' => 'required',
            'direccion' => 'required|max:255',
            'telefono' => 'required',
            'email' => 'required'
        ]);

        $cliente = Cliente::create($request->all());
        
        return redirect()
            ->route('cliente.show', $cliente->id)
            ->with('mensaje', 'Cliente creado con éxito')
        ;
    }

    /**
     * Cargar formulario para editar
     * @param int $id
     */
    public function edit(int $id) {
        $cliente = Cliente::find($id);

        return view('cliente.edit', compact('cliente'));
    }

    /**
     * Edita un recurso en DB.
     *
     * @param Request $request descripción
     * @param Cliente $cliente descripción
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, Cliente $cliente) {
        $request->validate([
            'nombre' => 'required',
            'apellidos' => 'required',
            'direccion' => 'required|max:255',
            'telefono' => 'required',
            'email' => 'required|email',
        ]);

        $cliente->update($request->all());

        return redirect()
            ->route('cliente.show', $cliente->id)
            ->with('mensaje', 'Cliente actualizado con éxito')
        ;
    }

    /**
     * Elimina un recurso de DB.
     *
     * @param Cliente $cliente descripcióncripción de la excepción
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Cliente $cliente) {
        $cliente->delete();
        
        return redirect()
            ->route('cliente.index')
            ->with('mensaje', 'Cliente borrado con éxito')
        ;
    }
}
